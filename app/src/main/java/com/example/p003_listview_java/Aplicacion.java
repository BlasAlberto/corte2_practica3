package com.example.p003_listview_java;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.example.p003_listview_java.Modelo.AlumnosDB;

import java.util.ArrayList;

public class Aplicacion extends Application {
    public static ArrayList<AlumnoItem> alumnos;
    private static AlumnoAdapter adaptador;

    public ArrayList<AlumnoItem> getAlumnos() { return alumnos; }
    public AlumnoAdapter getAdaptador() { return adaptador; }

    static AlumnosDB alumnosDb;

    @Override
    public void onCreate() {
        super.onCreate();

        alumnosDb = new AlumnosDB(getApplicationContext());
        alumnos = alumnosDb.allAlumnos();

        Log.d("", "onCreate: Tamaño array list: " + alumnos.size());
        adaptador = new AlumnoAdapter(alumnos, this);
    }
}
