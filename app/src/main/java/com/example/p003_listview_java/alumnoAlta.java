package com.example.p003_listview_java;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class alumnoAlta extends AppCompatActivity {

    // COMPONENTES DEL LAYOUT
    private Button btnGuardar, btnRegresar, btnEliminar;
    private AlumnoItem alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private int posicion;

    // CONSTANTES
    private static final int Pedido_Imagen = 1;

    // VARIABLES
    private Uri ruta = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno_alta);

        this.instanciarComponentes();

        Bundle paquete = getIntent().getExtras();
        this.alumno = (AlumnoItem) paquete.getSerializable("alumno");
        this.posicion = paquete.getInt("posicion", posicion);

        if(posicion >= 0){
            this.txtNombre.setText(this.alumno.getNombre());
            this.txtMatricula.setText(this.alumno.getMatricula());
            this.txtGrado.setText(this.alumno.getCarrera());

            // Cargar imagen
            Bitmap bitmap = BitmapFactory.decodeByteArray(alumno.getImagen(), 0, alumno.getImagen().length);
            this.imgAlumno.setImageBitmap(bitmap);
        }

        this.asignarEventosClic();
    }


    // INSTANCIAR LOS COMPONENTES DEL LAYOUT
    private void instanciarComponentes()
    {
        this.btnGuardar = findViewById(R.id.btnGuardar);
        this.btnRegresar = findViewById(R.id.btnRegresar);
        this.btnEliminar = findViewById(R.id.btnEliminar);

        this.txtNombre = findViewById(R.id.txtNombre);
        this.txtMatricula = findViewById(R.id.txtMatricula);
        this.txtGrado = findViewById(R.id.txtGrado);

        this.imgAlumno = findViewById(R.id.imgAlumno);
    }

    // ASIGNAR EVENTOS CLIC A LOS BOTONES
    private void asignarEventosClic()
    {
        this.btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnGuardar(); }
        });
        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnRegresar(); }
        });
        this.imgAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_imgAlumno(); }
        });

        if(this.alumno == null)
            this.btnEliminar.setEnabled(false);
        else {
            this.btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) { clic_btnEliminar(); }
            });
            Toast.makeText(getApplicationContext(), this.alumno.getId() + "", Toast.LENGTH_SHORT).show();
        }

    }


    // VALIDAR EL LLENADO DE VALORES
    public boolean validar(){
        Log.d("nombre", "validar" + txtNombre.getText());

        if(
                this.txtNombre.getText().toString().equals("") ||
                this.txtMatricula.getText().toString().equals("") ||
                this.txtGrado.getText().toString().equals("")
        ) return false;

        else return true;
    }


    // METODO CLIC PARA EL Button: btnGuardar
    private void clic_btnGuardar()
    {
        if(validar()){

            String strNombre = this.txtNombre.getText().toString();
            String strMatricula = this.txtMatricula.getText().toString();
            String strGrado = this.txtGrado.getText().toString();
            byte imagen[] = obtenerDatosImagen();

            // Si no se envio ni un alumno
            // Guardar uno nuevo
            if (this.alumno == null)
            {
                this.alumno = new AlumnoItem();
                this.alumno.setNombre(strNombre);
                this.alumno.setMatricula(strMatricula);
                this.alumno.setCarrera(strGrado);
                this.alumno.setImagen(imagen);

                Aplicacion.alumnosDb.insertAlumno(alumno);

                AlumnoItem nuevoAlumno = Aplicacion.alumnosDb.ultimoAlumno();
                Aplicacion.alumnos.add(nuevoAlumno);

                setResult(Activity.RESULT_OK);
                finish();
            }

            // Si se envio un alumno y una posición
            // Modificar el enviado
            else if(posicion >= 0)
            {
                this.alumno.setNombre(strNombre);
                this.alumno.setMatricula(strMatricula);
                this.alumno.setCarrera(strGrado);
                this.alumno.setImagen(imagen);

                Aplicacion.alumnosDb.updateAlumno(alumno);

                Aplicacion.alumnos.get(posicion).setNombre(strNombre);
                Aplicacion.alumnos.get(posicion).setMatricula(strMatricula);
                Aplicacion.alumnos.get(posicion).setCarrera(strGrado);
                Aplicacion.alumnos.get(posicion).setImagen(imagen);

                setResult(Activity.RESULT_OK);
                finish();
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "Falto capturar datos", Toast.LENGTH_SHORT).show();
            this.txtMatricula.requestFocus();
        }

    }

    // METODO CLIC PARA EL Button: btnEliminar
    private void clic_btnEliminar(){

        Aplicacion.alumnosDb.deleteAlumno(this.alumno.getId());

        Aplicacion.alumnos.remove(posicion);

        setResult(Activity.RESULT_OK);
        finish();
    }


    // METODO CLIC PARA EL Button: btnRegresa
    private void clic_btnRegresar(){
        finish();
    }


    // METODO CLIC PARA EL ImageView: imgAlumno
    private void clic_imgAlumno(){
        // Abrir selector de archivos
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, Pedido_Imagen);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if ( // Comprobar:
                requestCode == Pedido_Imagen    // El resultado proviene de la selección de archivos y sea una imagen
                && resultCode == RESULT_OK      // Resultado sea exitoso
                && data != null)                // Resultado no nulo
        {
            // Obtejener la ruta dela imagen seleccionada
            this.ruta = data.getData();
            // Cargar la imagen dentro del componente
            Picasso.get().load(this.ruta).into(this.imgAlumno);
        }
    }

    private byte[] obtenerDatosImagen() {
        byte[] datosImagen = null;

        try {

            Bitmap bitmap = ((BitmapDrawable) this.imgAlumno.getDrawable()).getBitmap();


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            datosImagen = stream.toByteArray();
            stream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return datosImagen;
    }



}