package com.example.p003_listview_java;

import java.io.Serializable;
import java.util.ArrayList;

public class AlumnoItem implements Serializable {
    private int id;
    // Componentes del Item
    private String carrera;
    private String matricula;
    private String nombre;
    private byte[] imagen;


    // Constructor
    public AlumnoItem(){
        this.carrera = "";
        this.matricula = "";
        this.nombre = "";
        this.imagen = new byte[0];
    }
    public AlumnoItem(String carrera, String matricula, String nombre, byte[] imagen){
        this.carrera = carrera;
        this.matricula = matricula;
        this.nombre = nombre;
        this.imagen = imagen;
    }


    // Encapsulamiento (GETs && SETs)
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getCarrera() { return carrera; }
    public void setCarrera(String carrera) { this.carrera = carrera; }
    public String getMatricula() { return matricula; }
    public void setMatricula(String matricula) { this.matricula = matricula; }
    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    public byte[] getImagen() { return imagen; }
    public void setImagen(byte[] imagen) { this.imagen = imagen; }
}
