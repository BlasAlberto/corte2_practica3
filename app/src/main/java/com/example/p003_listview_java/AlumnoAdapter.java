package com.example.p003_listview_java;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AlumnoAdapter extends RecyclerView.Adapter<AlumnoAdapter.ViewHolder> implements View.OnClickListener{

    private ArrayList<AlumnoItem> infAlumnos;   // Lista con la información de cada alumno o elemento
    private ArrayList<AlumnoItem> listaFinal;   // Lista que se mostrara;
    private LayoutInflater inflador;            // Clase que convierte en objetos nuestras vistas
    private View.OnClickListener listener;



    public AlumnoAdapter(ArrayList<AlumnoItem> listaAlumnos, Application contexto){
        this.infAlumnos = new ArrayList<>();
        this.infAlumnos.addAll(listaAlumnos);
        this.listaFinal = listaAlumnos;
        this.inflador = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflador.inflate(R.layout.layout_itemalumno, null);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlumnoAdapter.ViewHolder holder, int position) {
        AlumnoItem alumno = this.listaFinal.get(position);

        holder.lblNombre.setText(alumno.getNombre());
        holder.lblCarrera.setText(alumno.getCarrera());
        holder.lblMatricula.setText(alumno.getMatricula());

        // Cargar imagen
        Bitmap bitmap = BitmapFactory.decodeByteArray(alumno.getImagen(), 0, alumno.getImagen().length);
        holder.idImagen.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return this.listaFinal.size();
    }

    @Override
    public void onClick(View view) {
        if (listener != null)
            listener.onClick(view);
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView lblNombre;
        private TextView lblMatricula;
        private TextView lblCarrera;
        private ImageView idImagen;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.lblNombre = itemView.findViewById(R.id.lblNombreAlumno);
            this.lblMatricula = itemView.findViewById(R.id.lblMatricula);
            this.lblCarrera = itemView.findViewById(R.id.lblCarrera);
            this.idImagen = itemView.findViewById(R.id.imgvAlumno);
        }
    }








    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence texto)
            {
                // Creación del objeto que contendra el filtrado
                FilterResults resultado = new FilterResults();

                // Creación de lista filtrada
                ArrayList<AlumnoItem> listaFiltrada = new ArrayList<>();

                // Comprobar si se hizo una busqueda
                // Comprobar si hay caracteres
                if(texto!=null && texto.length()!=0)
                {
                    // Se limpia la busqueda
                    String busqueda = texto.toString().toLowerCase().trim();

                    // Comprobar en cada alumno sí coincide con la busqueda
                    for(AlumnoItem item: infAlumnos)
                    {
                        // Se limpia el nombre y matricula
                        String nombre = item.getNombre().toLowerCase().trim();
                        String matricula = item.getMatricula().toLowerCase().trim();

                        // Sí coincide, se agrega
                        if(nombre.contains(busqueda) || matricula.contains(busqueda))
                            listaFiltrada.add(item);
                    }
                }

                // Sí no, se descarta el filtrado
                // La lista contendra a todos los alumnos
                else listaFiltrada.addAll(infAlumnos);

                // Guardar los resultados
                resultado.values = listaFiltrada;
                resultado.count = listaFiltrada.size();

                return resultado;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults resultado) {
                // Preparar la lista con los alumnos filtrados
                listaFinal.clear();
                listaFinal.addAll((ArrayList<AlumnoItem>) resultado.values);
                // Notificar de los cambios
                // Actualizar los alumnos a mostrar
                notifyDataSetChanged();
            }
        };
    }
}
