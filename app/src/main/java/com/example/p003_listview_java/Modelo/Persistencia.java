package com.example.p003_listview_java.Modelo;

import com.example.p003_listview_java.AlumnoItem;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertAlumno(AlumnoItem alumno);
    public long updateAlumno(AlumnoItem alumno);
    public void deleteAlumno(int id);

}
