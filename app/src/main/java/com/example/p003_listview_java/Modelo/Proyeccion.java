package com.example.p003_listview_java.Modelo;

import android.database.Cursor;

import com.example.p003_listview_java.AlumnoItem;

import java.util.ArrayList;

public interface Proyeccion {
    public AlumnoItem getAlumno(String matricula);
    public ArrayList<AlumnoItem> allAlumnos();
    public AlumnoItem readAlumno(Cursor cursor);

    public AlumnoItem ultimoAlumno();

}
