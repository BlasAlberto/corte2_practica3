package com.example.p003_listview_java;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.p003_listview_java.Modelo.AlumnosDB;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private RecyclerView lvAlumnos;
    private RecyclerView.LayoutManager layoutManager;
    private Aplicacion app;
    private FloatingActionButton fbtnAgregar;

    private AlumnoItem alumno;
    private int posicion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.instanciarObjetos();

        this.fbtnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_fbtnAgregar(); }
        });

        app.getAdaptador().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_lvAlumnos(view); }
        });
    }



    private void instanciarObjetos(){
        this.alumno = new AlumnoItem();
        this.posicion = -1;

        this.app = (Aplicacion) getApplication();

        this.layoutManager = new LinearLayoutManager(this);

        this.lvAlumnos = findViewById(R.id.lvAlumnos);
        this.lvAlumnos.setAdapter(app.getAdaptador());
        this.lvAlumnos.setLayoutManager(this.layoutManager);

        this.fbtnAgregar = findViewById(R.id.btnAgregarAlumno);
    }

    public boolean onCreateOptionsMenu(Menu menu){
        // Inflar el menu
        MenuInflater inflador = getMenuInflater();
        inflador.inflate(R.menu.layout_buscaralumno, menu);

        // Obtener el item encargado de buscar
        MenuItem itemBuscar = menu.findItem(R.id.menuAlumnos);

        // Obtener vista asociada al item
        SearchView buscador = (SearchView) itemBuscar.getActionView();

        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) { return false; }

            @Override
            public boolean onQueryTextChange(String s) {
                app.getAdaptador().getFilter().filter(s);

                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    // METODO CLIC PARA EL Button: fbtnAgregar
    private void clic_fbtnAgregar()
    {
        this.alumno = null;

        Bundle paquete = new Bundle();
        paquete.putSerializable("alumno", this.alumno);
        paquete.putInt("posicion", this.posicion);

        Intent intent = new Intent(MainActivity.this, alumnoAlta.class);
        intent.putExtras(paquete);

        startActivityForResult(intent, 0);
    }

    private void clic_lvAlumnos(View view){
        posicion = lvAlumnos.getChildAdapterPosition(view);
        alumno = app.getAlumnos().get(posicion);

        Bundle paquete = new Bundle();
        paquete.putSerializable("alumno", alumno);
        paquete.putInt("posicion", posicion);

        Intent intent = new Intent(MainActivity.this, alumnoAlta.class);
        intent.putExtras(paquete);

        startActivityForResult(intent, 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        lvAlumnos.getAdapter().notifyDataSetChanged();
        this.posicion = -1;
        this.alumno = null;
    }
}